<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');


Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('/hello', function () {
    return 'Hello Larevel';
});


    
Route::get('/comment{id}', function ($id) {
    return view('comment', compact('id'));
});

//ex5
Route::get('/users/{name?}/{email}', function($name = '"name missing"',$email){ 
    return view('users', compact('email','name'));
});





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
